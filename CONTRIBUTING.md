Code Contribution Guidelines
===============

Introduction
---------------

This short guide provides instructions for writers that plan to contribute to this book.

The Digital Permaculture for All project was started by Aimee Fenech first as an idea and some draft structure which then spread a little further to include others working in this domain.

As a principle we acknowledge that external contributions will require assitance to become involved and contribute their experience and knowledge towards this topic. This document seekes to provide anchor points to which contributors can orient themselves toward in their writing.

Communication Channels
---------------

There are two communication channels for writers:

- Matrix room: https://matrix.to/#/#digitalpermaculture:matrix.org is on-line chat room that is dedicated to interactive communication among writers. One of the primary purposes of this chat is to support communication about writing contributions.
- Email: aimee@ecohackerfarm.org is used for one-to-one communication about vision overall direction of the book.
- Mailing list: not yet set up

Please feel free to use any of those communication channels. Communication about writing contributions welcome. Please keep in mind that communication in matrix is public. Communication is expected to be kind and civil. 

Accepting Contributions
---------------

The Digital Permaculutre for All project is open to contributions of any kind. However, there are two critical criteria for accepting a contribution:

- Quality: Contribution quality should be appropriate. Writing will be edited to keep the book coherent and in a consistent voice. Content will be fact checked, referenced and may be rejected if found to be unsubstantiated.

- Licensing: [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/). If you do not agree with this license choice please refrain from contributing to this project.

Discuss The Contribution
---------------

First and most important guideline: **Discuss your contribution before starting the work**. Let us know that you plan to contribute before you do any writing. Use the matrix chat. This will help not duplicate work and avoid situations where contributions have to be rejected.

Content of Contribution
---------------

Contribution should contain:

- Text for the book: This is the "core" of the contribution. 
- References: any reference needs to be marke as such using [Bibtex style](https://www.overleaf.com/learn/latex/Bibtex_bibliography_styles) 
- Glossary: any terms need to be linked to either existing terms or you need a to create a new term

\* TODO here: how to add a term to Glossary
















